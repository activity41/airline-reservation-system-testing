package airline.reservation.testing.AlFareed_Flight;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.ITestListener;

public class CustomListener implements ITestListener {
    public void onStart(ITestContext arg) {
        System.out.println("Start testing..." + arg.getName());
    }

    public void onFinish(ITestContext arg) {
        System.out.println("Finished Test execution..." + arg.getName());

    }

    public void onTestStart(ITestResult arg0) {
        System.out.println("Test started for - " + arg0.getName());
    }

    public void onTestSkipped(ITestResult arg0) {
        System.out.println("Test Skipped - " + arg0.getName());

    }

    public void onTestSuccess(ITestResult arg0) {
        System.out.println("Test Succeeded - " + arg0.getName());

    }

    public void onTestFailure(ITestResult arg0) {
        System.out.println("Test Failed - " + arg0.getName());

    }

}
