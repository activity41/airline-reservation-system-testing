package airline.reservation.testing.AlFareed_Flight;
import airline.reservation.testing.Flight;
import java.time.LocalDate;
import java.time.LocalTime;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FlightTest {
    private Flight flight = new Flight();
   private Flight invalidFlightDetails;
    @BeforeClass(groups = "regression")
    public void setup(){
      flight =   new Flight("AXEF302","Indigo","Bangalore","Delhi",50,LocalDate.of(2023, 06, 01),LocalTime.of(10, 30));
    }

    @Test(groups = "regression")
    public void testGetters(){
        Assert.assertEquals(flight.getFlightNo(), "AXEF302");
        Assert.assertEquals(flight.getFligthName(), "Indigo");
        Assert.assertEquals(flight.getFromLoc(), "Bangalore");
        Assert.assertEquals(flight.getToLoc(), "Delhi");
        Assert.assertEquals(flight.getFlightCapacity(), 50);
        Assert.assertEquals(flight.getFlyingDate(), LocalDate.of(2023, 06, 01));
        Assert.assertEquals(flight.getFlyingTime(), LocalTime.of(10, 30));
    }
    @Test(groups = "failed",expectedExceptions = IllegalArgumentException.class, enabled = false)
    public void testInvalidInputs(){
        invalidFlightDetails =   new Flight("ABC1234",null,"Bangalore","Delhi",50,LocalDate.of(2023, 06, 01),LocalTime.of(10, 30));

    }
    
    @Test(groups = "regression")
    public void testSetters(){
        flight.setFlightNo("ADE781");
        flight.setFligthName("ETIHAD");
        flight.setFromLoc("Mumbai");
        flight.setToLoc("Goa");
        flight.setFlightCapacity(20);
        flight.setFlyingDate(LocalDate.of(2023, 9, 24));
        flight.setFlyingTime(LocalTime.of(10, 00));

        Assert.assertEquals(flight.getFlightNo(), "ADE781");
        Assert.assertEquals(flight.getFligthName(), "ETIHAD");
        Assert.assertEquals(flight.getFromLoc(), "Mumbai");
        Assert.assertEquals(flight.getToLoc(), "Goa");
        Assert.assertEquals(flight.getFlightCapacity(), 20);
        Assert.assertEquals(flight.getFlyingDate(), LocalDate.of(2023, 9, 24));
        Assert.assertEquals(flight.getFlyingTime(), LocalTime.of(10, 00));
    }
}
