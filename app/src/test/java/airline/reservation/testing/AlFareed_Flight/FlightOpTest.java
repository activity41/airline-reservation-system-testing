package airline.reservation.testing.AlFareed_Flight;

import airline.reservation.testing.Flight;
import airline.reservation.testing.FlightOp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FlightOpTest {
    private FlightOp flightOp = new FlightOp();
    private List<Flight> flights = new ArrayList<>();
    private Flight flight1;

    @BeforeClass(groups = "regression")
    public void setup() {
        flight1 = new Flight("AB12X", "KingFisher", "Mumbai", "Bangalore", 50, LocalDate.of(2022, 10, 10),
                LocalTime.of(11, 11, 00));
        Flight flight2 = new Flight("AXF12", "Indigo", "Bangalore", "Hyderabad", 50, LocalDate.of(2023, 9, 1),
                LocalTime.of(01, 00, 00));
        flights.add(flight1);
        flights.add(flight2);
    }

    @Test(groups = "regression")
    public void testFindFlights() {
        Flight testFlight = flightOp.findFlight(flights, "AB12X");
        Assert.assertEquals(testFlight, flight1);

    }

    @Test(groups = "regression")
    public void testRemoveflight() {
        Assert.assertTrue(flightOp.removeFlight(flights, "AB12X"));
    }

    @AfterClass
    public void remove() {
        flightOp = null;
        flights = null;
        flight1 = null;
    }

}
