package airline.reservation.testing.Amogh_Passenger;
//import org.junit.jupiter.api.Test;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import airline.reservation.testing.PassengerBookingDetails;
import airline.reservation.testing.Flight;
import airline.reservation.testing.Passenger;
import java.time.LocalDate;
import java.time.LocalTime;

public class PassengerTest {
    //testing assertEquals (objects)
    private Passenger passenger;
    private Flight flight;
    @Test()
    public void PassengerDetailsTest(){
        Assert.assertEquals(passenger.getName(),"al-fareed");
        Assert.assertEquals(passenger.getEmail(),"alf@gmail.com");
        Assert.assertEquals(passenger.getPassword(),"123lol");
        Assert.assertEquals(passenger.getAddress(),"Udupi");
        Assert.assertEquals(passenger.getMobile(),1234567890);
    }
    @Test
    public void BookingsDeatilsTest(){
        Assert.assertEquals(flight.getFligthName(),"imperial flights");
        Assert.assertEquals(flight.getFlightNo(),"QP123");
        Assert.assertEquals(flight.getFlightCapacity(),500);
        Assert.assertEquals(flight.getFlyingDate(),LocalDate.of(2023,5,6));
        Assert.assertEquals(flight.getFlyingTime(), LocalTime.of(4,30,0));
        Assert.assertEquals(flight.getFromLoc(),"Bangalore");
        Assert.assertEquals(flight.getToLoc(),"Mumbai");
    }
    @BeforeMethod
    public void setUp(){
        flight=new Flight("QP123","imperial flights","Bangalore","Mumbai",500,LocalDate.of(2023,5,6),LocalTime.of(4,30,0));
        passenger=new Passenger("al-fareed","alf@gmail.com","123lol","Udupi",1234567890);
    }
    @AfterMethod
    public void tearDown(){
        flight=null;
        passenger=null;
    }
    @Test(expectedExceptions = {IllegalArgumentException.class, NullPointerException.class})
    public void throwExceptionOnInvalidEmail(){
        Passenger passenger=new Passenger("al-fareed","alfgmailcom","123lol","Udupi",1234567890);
        Passenger passenger1=new Passenger(null,"alf@gmail.com","123lol","Udupi",1234567890);
        throw new IllegalArgumentException();
    }

}

