package airline.reservation.testing.Amogh_Passenger;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import airline.reservation.testing.PassengerBookingDetails;
import airline.reservation.testing.Flight;
import airline.reservation.testing.Passenger;
import java.time.LocalDate;
import java.time.LocalTime;

@Listeners(testListeners.class)
public class PassengerBookingTests {

    private PassengerBookingDetails passengerBookingDetails = new PassengerBookingDetails();
    private Flight flight = new Flight();
    private Passenger passenger = new Passenger();

    

    @Test(expectedExceptions = IllegalArgumentException.class, groups = "regression")
    public void modifyBookingsTests() {
        flight = new Flight(null, null, null, null, 0, LocalDate.of(2023, 12, 6), LocalTime.of(4, 30, 0));
        passenger = new Passenger("al-fareed", "alfgmail.com", "123lol", "Udupi", 1234567890);
        PassengerBookingDetails flightBooking = new PassengerBookingDetails(flight, passenger);
        passengerBookingDetails.addBooking(flightBooking);
        LocalDate modifiedDate = flight.getFlyingDate();
        Assert.assertEquals(flightBooking.getBookedDate(), modifiedDate);
        String modifiedToLoc = flight.getToLoc();
        Assert.assertEquals(flightBooking.getFlightDetails().getToLoc(), modifiedToLoc);
        String flightNum = flight.getFlightNo();
        Assert.assertEquals(flightBooking.getFlightDetails().getFlightNo(), flightNum);
    }

    @BeforeMethod
    public void setUp() {
        passengerBookingDetails = new PassengerBookingDetails();
        flight = new Flight();
    }

    @AfterMethod
    public void tearDown() {
        passengerBookingDetails = null;
        flight = null;
    }

    @Test(expectedExceptions = IllegalArgumentException.class, groups = "regression")
    public void testPassengers() {
        flight = new Flight("QP123", null, null, null, 0, LocalDate.of(2023, 12, 6), LocalTime.of(4, 30, 0));
        passenger = new Passenger(null, "abc123", "123lol", "Udupi", 1234567890);
        PassengerBookingDetails flightBooking = new PassengerBookingDetails(flight, passenger);
        passengerBookingDetails.addBooking(flightBooking);
        String email = passenger.getEmail();
        Assert.assertEquals(flightBooking.getPassengerDetails().getEmail(), email);
        String name = passenger.getName();
        Assert.assertEquals(flightBooking.getPassengerDetails().getName(), name);
    }
}
