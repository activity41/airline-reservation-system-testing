package airline.reservation.testing.Amogh_Passenger;

import org.testng.ITestListener;
import org.testng.ITestResult;

public class testListeners implements ITestListener {
    @Override
    public void onTestStart(ITestResult result){
        System.out.println("The test is beginning " +result.getTestName());
    }
    @Override
    public void onTestSuccess(ITestResult result){
        System.out.println("The test was successfully completed : "+result.getTestName()+ " with status "+result.getStatus());
    }
    @Override
    public void onTestFailure(ITestResult result){
        System.out.println("The current test failed with status "+result.getTestName());
    }
}
