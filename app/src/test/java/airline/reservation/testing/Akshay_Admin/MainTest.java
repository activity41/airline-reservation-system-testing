package airline.reservation.testing.Akshay_Admin;

import java.util.ArrayList;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import airline.reservation.testing.Passenger;

public class MainTest {
    //    private Passenger passenger = new Passenger();
    ArrayList<Passenger> passengerList;
    //    ArrayList<Flight> flightsList;
    @Test(groups = "regression")
    public void initialization(){
        ArrayList<Passenger> passengerList = new ArrayList<>();
        Passenger p1 = new Passenger("John Doe", "john@example.com", "password", "123 Main St", 1234567890L);
        Passenger p2 = new Passenger("Don Joe", "Don@example.com", "pasword", "122 cross St", 1234567800);
        Passenger p3 = new Passenger("Tom Doe", "Tom@example.com", "Tomword", "69 Main St", 3334567890L);
        passengerList.add(p1);
        passengerList.add(p2);
        passengerList.add(p3);
    }
    @Test(groups = "failed",expectedExceptions = IllegalArgumentException.class)
    public void nullInitializationTest(){
        ArrayList<Passenger> passengerList = new ArrayList<>();
        Passenger p4 = new Passenger(null, "Tom@example.com", "Tomword", "69 Main St", 3334567890L);
        passengerList.add(p4);
    }
    
}


