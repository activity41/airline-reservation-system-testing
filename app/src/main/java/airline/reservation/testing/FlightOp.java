package airline.reservation.testing;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

public class FlightOp {
    Scanner sc = new Scanner(System.in);
    public void viewAllFlights(List<Flight> flights) {
        System.out
                .println("------------------------------------------------------------------------------------------");

        System.out.printf("%-6s %-10s %-13s %-10s %-10s %-10s ", "Sl No.", "Flight No.", "Flight Name",
                "Flying Date", "Flying Time", "Capacity");
        int i = 1;
        System.out
                .println(
                        "\n------------------------------------------------------------------------------------------");
        for (Flight flight : flights) {
            System.out.printf("%-6s %-10s %-13s %-10s %-10s %-10s\n", i, flight.getFlightNo(),
                    flight.getFligthName(),
                    flight.getFlyingDate(), flight.getFlyingTime(), flight.getFlightCapacity());
            i++;
        }
        System.out
                .println("------------------------------------------------------------------------------------------");

    }

    public void viewFlightDetails(List<Flight> flights) {
        System.out
                .println("------------------------------------------------------------------------------------------");

        System.out.printf("%-6s %-10s %-13s %-10s %-10s %-13s %-13s %-10s ", "Sl No.", "Flight No.", "Flight Name",
                "From", "To",
                "Flying Date", "Flying Time", "Capacity");
        int i = 1;
        System.out
                .println(
                        "\n------------------------------------------------------------------------------------------");
        for (Flight flight : flights) {
            System.out.printf("%-6s %-10s %-13s %-10s %-10s %-13s %-13s %-10s\n", i, flight.getFlightNo(),
                    flight.getFligthName(), flight.getFromLoc(), flight.getToLoc(),
                    flight.getFlyingDate(), flight.getFlyingTime(), flight.getFlightCapacity());
            i++;
        }
        System.out
                .println("------------------------------------------------------------------------------------------");

    }
// gives complete details of flights to admin
    public void checkFlightDetails(List<Flight> flights) {
        System.out.println("Enter the flight no : ");
        String flightNo = sc.nextLine();
        System.out.printf("%-10s %-13s %-10s %-10s %-13s %-13s %-10s ", "Sl No.", "Flight No.", "Flight Name",
                "From", "To",
                "Flying Date", "Flying Time", "Capacity");
                 System.out
                .println("\n------------------------------------------------------------------------------------------");

        for (Flight flight : flights) {
            if(flight.getFlightNo().equals(flightNo)){
                System.out.printf("%-10s %-13s %-10s %-10s %-13s %-13s %-10s\n", flight.getFlightNo(),
                    flight.getFligthName(), flight.getFromLoc(), flight.getToLoc(),
                    flight.getFlyingDate(), flight.getFlyingTime(), flight.getFlightCapacity());
            
            }
        }
    }
    public Flight findFlight(List<Flight> flights,String flightNum){
        for (Flight flight : flights) {
            if(flight.getFlightNo().equals(flightNum)){
                return flight;
            }
        }
        return null;
    }
    public boolean removeFlight(List<Flight> flights,String flightNum){
        for (Flight flight : flights) {
            if(flight.getFlightNo().equals(flightNum)){
                flights.remove(flight);
                System.out.println("Removed flight successfully..");
                return true;
            }
        }
        System.out.println("Flight doesnot exiist..");
        return false;
    }
    public void updateFlight(List<Flight> flights){
        System.out.println("Enter the flight number : ");
        String flightNum = sc.next();
        for (Flight flight : flights) {
            if(flight.getFlightNo().equals(flightNum)){
                System.out.println("--Update Flight--\n1.From\n2.To\n3.Date\n4.Time\n5.Capacity\n6.Mass Update\nChoose one option : ");
                int choice = sc.nextInt();
                switch(choice){
                    case 1: System.out.println("Enter the departure location : ");
                            flight.setFromLoc(sc.next());
                            System.out.println("Departure Location has been successfully updated..");
                            break;
                    case 2: System.out.println("Enter the destination location : ");
                            flight.setToLoc(sc.next());
                            System.out.println("Destination location has been updated Successfully..");
                            break;
                    case 3: System.out.println("Enter the Date of departure(dd mm yyyy): ");
                            flight.setFlyingDate(LocalDate.of(sc.nextInt(), sc.nextInt(), sc.nextInt()));
                            System.out.println("Departure date has been updated Successfully..");
                            break;
                    case 4: System.out.println("Enter the departure time (hh mm ss) : ");
                            flight.setFlyingTime(LocalTime.of(sc.nextInt(), sc.nextInt(), sc.nextInt()));
                            System.out.println("Departure time has been updaed Successfully..");
                            break;
                    case 5: System.out.println("Enter the number of capacity : ");
                            int capacity = sc.nextInt();
                            flight.setFlightCapacity(capacity);
                            break;
                    case 6: System.out.println("Enter the flight details(From To Date Time Capacity)");
                            flight.setFromLoc(sc.next());
                            flight.setToLoc(sc.next());
                            flight.setFlyingDate(LocalDate.of(sc.nextInt(), sc.nextInt(), sc.nextInt()));
                            flight.setFlyingTime(LocalTime.of(sc.nextInt(), sc.nextInt(), sc.nextInt()));
                            flight.setFlightCapacity(sc.nextInt());
                            break;
                    default: System.out.println("Invalid choice..!");
                }
            }
        }
        
    }
}

