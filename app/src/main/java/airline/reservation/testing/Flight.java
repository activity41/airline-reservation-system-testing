package airline.reservation.testing;
import java.time.LocalDate;
import java.time.LocalTime;

public class Flight {
    private String flightNo, fligthName, FromLoc, ToLoc;
    private int flightCapacity;
    LocalDate flyingDate;
    LocalTime flyingTime;

    public Flight() {
        
    }
    public Flight(String flightNo, String fligthName, String fromLoc, String toLoc, int flightCapacity,
        LocalDate flyingDate, LocalTime flyingTime) {
        this.flightNo = flightNo;
        this.fligthName = fligthName;
        FromLoc = fromLoc;
        ToLoc = toLoc;
        this.flightCapacity = flightCapacity;
        this.flyingDate = flyingDate;
        this.flyingTime = flyingTime;
    }
    public Flight(String flightNo) {
        this.flightNo= flightNo;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getFligthName() {
        return fligthName;
    }

    public void setFligthName(String fligthName) {
        this.fligthName = fligthName;
    }

    public String getFromLoc() {
        return FromLoc;
    }

    public void setFromLoc(String fromLoc) {
        this.FromLoc = fromLoc;
    }

    public String getToLoc() {
        return ToLoc;
    }

    public void setToLoc(String toLoc) {
        this.ToLoc = toLoc;
    }

    public int getFlightCapacity() {
        return flightCapacity;
    }

    public void setFlightCapacity(int flightCapacity) {
        this.flightCapacity = flightCapacity;
    }

    public LocalDate getFlyingDate() {
        return flyingDate;
    }

    public void setFlyingDate(LocalDate flyingDate) {
        this.flyingDate = flyingDate;
    }

    public LocalTime getFlyingTime() {
        return flyingTime;
    }

    public void setFlyingTime(LocalTime flyingTime) {
        this.flyingTime = flyingTime;
    }

    public void display() {
        System.out.println("flight no " + this.fligthName);
    }
}
