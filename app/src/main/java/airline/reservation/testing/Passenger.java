package airline.reservation.testing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Passenger {
    private String name;
    private String email;
    private String password;
    private String address;
    private long mobile ;
    public Passenger() {
        super();
    }

    public long getMobile() {
        return mobile;
    }

    public void setMobile(long mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Passenger(String name, String email, String password, String address, long mobile) {
        this.name = name;
        this.email = email;
        if(!isValidEmail(email)){
            throw new IllegalArgumentException("Invalid email");
        }
        this.password = password;
        this.address = address;
        this.mobile = mobile;
        if(isValidPhoneNumber(mobile)){
            throw new IllegalArgumentException("Invalid Phone Number");
        }
    }

    public void display(){
        System.out.println("--Passenger Details--");
        System.out.println("Passenger Name : "+this.name);
        System.out.println("Passenger Email : "+this.email);
        System.out.println("Passenger Address  : "+this.address);
        System.out.println("Passenger mobile : "+this.mobile);
    }
    public boolean isValidPhoneNumber(long mobile){
        String regex="/^\\d{10}$/";
        Pattern pattern=Pattern.compile(regex);
        Matcher matcher= pattern.matcher(email);
        return matcher.matches();
    }
    public boolean isValidEmail(String email){
        String regex="^(.+)@(.+)$";
        Pattern pattern=Pattern.compile(regex);
        Matcher matcher=pattern.matcher(email);
        return matcher.matches();
    }
    
}
