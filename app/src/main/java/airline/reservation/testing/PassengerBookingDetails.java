package airline.reservation.testing;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class PassengerBookingDetails {
    Flight flightDetails;
    Passenger passengerDetails;
    LocalDate bookedDate;
    LocalTime bookedTime;
    List<PassengerBookingDetails> allBookings;

    

    public Flight getFlightDetails() {
        return flightDetails;
    }

    public void setFlightDetails(Flight flightDetails) {
        this.flightDetails = flightDetails;
    }

    public Passenger getPassengerDetails() {
        return passengerDetails;
    }

    public void setPassengerDetails(Passenger passengerDetails) {
        this.passengerDetails = passengerDetails;
    }

    public LocalDate getBookedDate() {
        return bookedDate;
    }

    public void setBookedDate(LocalDate bookedDate) {
        this.bookedDate = bookedDate;
    }

    public LocalTime getBookedTime() {
        return bookedTime;
    }

    public void setBookedTime(LocalTime bookedTime) {
        this.bookedTime = bookedTime;
    }

    public PassengerBookingDetails() {
        allBookings = new ArrayList<PassengerBookingDetails>();
    }

    public PassengerBookingDetails(Flight flightDetails, Passenger passengerDetails) {
        this.flightDetails = flightDetails;
        this.passengerDetails = passengerDetails;
        this.bookedDate = flightDetails.getFlyingDate();
        this.bookedTime = flightDetails.getFlyingTime();
    }

    public void addBooking(PassengerBookingDetails booking) {

        allBookings.add(booking);
    }

    // public void removeBooking(PassengerBookingDetails booking) {
    // if (allBookings.remove(booking)) {
    // System.out.println("Booking removed successfully.");
    // } else {
    // System.out.println("Failed to remove booking. Booking not found.");
    // }
    // }

    public void displayBookings() {
        Iterator<PassengerBookingDetails> iterator = allBookings.iterator();
        while (iterator.hasNext()) {
            PassengerBookingDetails p = iterator.next();
            p.passengerDetails.display();
            System.out.println("--Flight Details--");
            System.out.println("Flight Name : " + p.flightDetails.getFligthName());
            System.out.println("Flight No   : " + p.flightDetails.getFlightNo());
            System.out.println("Flight From : " + p.flightDetails.getFromLoc());
            System.out.println("Flight To   : " + p.flightDetails.getToLoc());
            System.out.println("Flight Date : " + p.flightDetails.getFlyingDate());
            System.out.println("Flight Time : " + p.flightDetails.getFlyingTime());
        }
    }


    public void displayOne() {
        passengerDetails.display();
        System.out.println("--Flight Details--");
        System.out.println("Flight Name : " + this.flightDetails.getFligthName());
        System.out.println("Flight No   : " + this.flightDetails.getFlightNo());
        System.out.println("Flight From : " + this.flightDetails.getFromLoc());
        System.out.println("Flight To   : " + this.flightDetails.getToLoc());
        System.out.println("Flight Date : " + this.flightDetails.getFlyingDate());
        System.out.println("Flight Time : " + this.flightDetails.getFlyingTime());
    }

    public void modifyBooking(String passengerEmail) {
        boolean bookingModified = false;
        for (PassengerBookingDetails booking : allBookings) {
            if (booking.passengerDetails.getEmail().equals(passengerEmail)) {
                LocalDate newDate = promptForNewDate();
                if (!newDate.equals(booking.bookedDate)) {
                    booking.bookedDate = newDate;
                    booking.flightDetails.setFlyingDate(newDate);
                    bookingModified = true;
                    System.out.println("Booking modified successfully for passenger " + passengerEmail
                            + ". New flight date: " + newDate);
                } else {
                    System.out.println("Booking modification failed. The new date is the same as the current date.");
                }
            }
        }
        if (!bookingModified) {
            System.out.println("No bookings found for passenger " + passengerEmail);
        }
    }

    private LocalDate promptForNewDate() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the new flight date (yyyy-MM-dd): ");
        String dateString = scanner.nextLine();
        return LocalDate.parse(dateString);
    }
}
