package airline.reservation.testing;

import java.util.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.time.LocalDate;
import java.time.LocalTime;

class AdminPassengerOperation {
    Scanner sc = new Scanner(System.in);
    operationAdmin oa = new operationAdmin();

    public void addPassenger(ArrayList<Passenger> passengers, ArrayList<Flight> flights) {
        System.out.print("Enter the passenger name : ");
        String name = sc.next();
        System.out.print("Enter the passenger email : ");
        String email = sc.next();
        System.out.print("Enter the passenger password : ");
        String pass = sc.next();
        System.out.print("Enter the passenger address : ");
        String address = sc.next();
        System.out.print("Enter the passenger mobile : ");
        long number = sc.nextLong();
        passengers.add(new Passenger(name, email, pass, address, number));
        System.out.println("Successfully Added");
        oa.start(passengers, flights);
    }

    public void displayPassenger(ArrayList<Passenger> passengers, ArrayList<Flight> flights) {
        int temp = passengers.size();
        System.out.println(String.format("%-10s %-15s %-10s %-10s", "Name", "Email",
                "Address", "Mobile"));
        System.out.println("---------------------------------------------------");
        for (int i = 0; i < temp; i++) {
            System.out.println(String.format("%-10s %-15s %-10s %-10s",
                    passengers.get(i).getName(), passengers.get(i).getEmail(),
                    passengers.get(i).getAddress(), passengers.get(i).getMobile()));
        }
         System.out.println("---------------------------------------------------");
    }

    public void removePassenger(ArrayList<Passenger> passengers, ArrayList<Flight> flights) {
        System.out.print("Enter the mail of the Passenger : ");
        String word = sc.next();
        int temp = passengers.size();
        for (int i = 0; i < temp; i++) {
            if (passengers.get(i).getEmail().equals(word)) {
                passengers.remove(i);
                break;
            }
        }
        oa.start(passengers, flights);
    }

    public void editPassenger(ArrayList<Passenger> passengers, ArrayList<Flight> flights) {
        System.out.println("Enter Passenger's Email");
        String word = sc.next();
        System.out.println("1.For changing Name");
        System.out.println("2.For changing Address");
        System.out.println("3.For changing Mobile Nuber");
        System.out.print("Choose the option to be updated : ");
        int choice = sc.nextInt();
        switch (choice) {
            case 1:
                System.out.print("Enter a new name : ");
                String updatedName = sc.next();
                for (Passenger p : passengers) {
                    if (p.getEmail().equals(word)) {
                        p.setName(updatedName);
                    }
                }
                break;
            case 2:
                System.out.print("Enter new Address : ");
                String updatedAddress = sc.next();
                for (Passenger p : passengers) {
                    if (p.getEmail().equals(word)) {
                        p.setAddress(updatedAddress);
                    }
                }
                break;
            case 3:
                System.out.print("Enter new Mobile Number : ");
                long updatedMobile = sc.nextLong();
                for (Passenger p : passengers) {
                    if (p.getEmail().equals(word)) {
                        p.setMobile(updatedMobile);
                    }
                }
                break;
        }

        oa.start(passengers, flights);
    }

    public void addFlight(ArrayList<Passenger> passengers, ArrayList<Flight> flights) {
        System.out.print("Enter the flight number : ");
        String flnu = sc.next();
        System.out.print("Enter the flight name : ");
        String name = sc.next();
        System.out.print("Enter the form location : ");
        String floc = sc.next();
        System.out.print("Enter the to location : ");
        String tloc = sc.next();
        System.out.print("Enter the flight capacity : ");
        int fcap = sc.nextInt();
        System.out.print("Enter the flying date(dd mm yyyy) : ");
        int d = sc.nextInt();
        int m = sc.nextInt();
        int y = sc.nextInt();
        LocalDate date = LocalDate.of(y, m, d);
        System.out.print("Enter the flying time(hh mm ss): ");
        int h = sc.nextInt();
        int mi = sc.nextInt();
        int s = sc.nextInt();
        LocalTime time = LocalTime.of(h, mi, s);
        flights.add(new Flight(flnu, name, floc, tloc, fcap, date, time));
        operationAdmin oa = new operationAdmin();
        oa.start(passengers, flights);
    }
}

class operationAdmin {

    FlightOp flightOperations = new FlightOp();
    Scanner sc = new Scanner(System.in);

    public void start(ArrayList<Passenger> passengers, ArrayList<Flight> flights) {
        initiate ink = new initiate();
        int choice;
        AdminPassengerOperation apo = new AdminPassengerOperation();
        System.out.println("Welcome to Elite Airlines Admin Section");
        do {
            System.out.println("1.Add Passenger");
            System.out.println("2.Edit Passenger");
            System.out.println("3.Remove Passenger");
            System.out.println("4.Display all Passenger List");
            System.out.println("5.Add Flight");
            System.out.println("6.Remove Flight");
            System.out.println("7.Update Flight");
            System.out.println("8.Display all Flight");
            System.out.println("9.Exit");
            System.out.print("Choose an option : ");
            choice = sc.nextInt();
            switch (choice) {
                case 1:
                    apo.addPassenger(passengers, flights);
                    break;
                case 2:
                    apo.editPassenger(passengers, flights);
                    break;
                case 3:
                    apo.removePassenger(passengers, flights);
                    break;
                case 4:
                    apo.displayPassenger(passengers, flights);
                    break;
                case 5:
                    apo.addFlight(passengers, flights);
                    break;
                case 6:
                    System.out.print("Enter Flight Number : ");
                    String flightnumber = sc.next();
                    flightOperations.removeFlight(flights, flightnumber);
                    break;
                case 7:
                    flightOperations.updateFlight(flights);
                    break;
                case 8:
                    flightOperations.viewFlightDetails(flights);
                    break;
            }
        } while (choice != 9);
        ink.start(passengers, flights);
    }
}

class admin {

    Scanner sc = new Scanner(System.in);

    public void start(ArrayList<Passenger> passengers, ArrayList<Flight> flights) {
        // System.out.println("Welcome to Elite Airlines");
        System.out.print("Please enter your password : ");
        int password = sc.nextInt();
        if (password == 6946) {
            operationAdmin opadmi = new operationAdmin();
            opadmi.start(passengers, flights);
        } else {
            start(passengers, flights);
        }
    }
}

class pasenger {
    initiate ink = new initiate();

    public Passenger getOnePassenger(List<Passenger> pas, String email) {
        for (Passenger p : pas) {
            if (p.getEmail().equals(email)) {
                return p;
            }
        }
        return null;
    }

    public void start(ArrayList<Passenger> passengers, ArrayList<Flight> flights) {
        FlightOp flightOp = new FlightOp();
        PassengerBookingDetails passengerBookingDetails = new PassengerBookingDetails();
        AdminPassengerOperation apo = new AdminPassengerOperation();
        Scanner sc = new Scanner(System.in);
        int choice;
        do {
            System.out.println(
                "1.View All Flights\n2.Book Flights\n3.View Booked Flights\n4.Cancel Flight \n5.Modify Ticket\n6.Edit Profile\n7.Exit");
                System.out.print("Enter your choice : "); 
            choice = sc.nextInt();
            switch (choice) {
                case 1:
                    flightOp.viewFlightDetails(flights);
                    break;
                case 2:
                    System.out.print("Enter the Flight Number : ");
                    String flightNumber = sc.next();
                    System.out.print("Enter the Passenger Email-Id : ");
                    String email = sc.next();
                    Flight fli = flightOp.findFlight(flights, flightNumber);
                    Passenger pas = getOnePassenger(passengers, email);
                    if (pas != null && fli != null) {
                        PassengerBookingDetails pDetails = new PassengerBookingDetails(fli, pas);
                        passengerBookingDetails.addBooking(pDetails);
                    } else {
                        System.out.println("Invalid Inputs..!");
                    }
                    break;
                case 3:
                    passengerBookingDetails.displayBookings();
                    break;
                case 4:
                    System.out.print("Enter the Flight Number : ");
                    String flightNumber1 = sc.next();
                    System.out.print("Enter the Passenger Email-Id : ");
                    String email1 = sc.next();
                    Flight fli1 = flightOp.findFlight(flights, flightNumber1);
                    if(fli1==null){
                        System.out.println("Flight does not exist..!");
                        return;
                    }
                    Passenger pas1 = getOnePassenger(passengers, email1);
                    Iterator<PassengerBookingDetails> iterator = passengerBookingDetails.allBookings.iterator();
                    while (iterator.hasNext()) {
                        PassengerBookingDetails p = iterator.next();
                        if (p.passengerDetails.getEmail().equals(pas1.getEmail())) {
                            iterator.remove(); // Safely remove the element using the iterator
                            break;
                        }
                    }
                    break;
                case 5:
                    System.out.println("Enter email");
                    String email2 = sc.next();
                    passengerBookingDetails.modifyBooking(email2);
                    break;
                case 6:
                    apo.editPassenger(passengers, flights);
                    break;
                case 7:
                    ink.start(passengers, flights);
                    break;
                case 8:break;
                 default : System.out.println("Invalid Choice");
            }
        } while (choice != 8);

    }
}

class initiate {
    Scanner sc = new Scanner(System.in);

    public void start(ArrayList<Passenger> passengers, ArrayList<Flight> flights) {
        int choice ;
        boolean flag = true;
        System.out.println("---Welcome to Elite Airlines---");
        while(flag){
            System.out.println("1.Admin");
            System.out.println("2.Passenger");
            System.out.print("3.Exit\nChoose the type of user : ");
            choice = sc.nextInt();
            switch (choice) {
                case 1:
                    admin admin = new admin();
                    admin.start(passengers, flights);
                    break;
                case 2:
                    pasenger pas = new pasenger();
                    pas.start(passengers, flights);
                    break;
                case 3:flag = false;
                return;
                default:System.out.println("Invalid choice..!");
            }
        }
    }
}

public class Main {
    public static void main(String[] args) {
        ArrayList<Passenger> passengers = new ArrayList<Passenger>();
        ArrayList<Flight> flights = new ArrayList<Flight>();
        Passenger p = new Passenger("Ajay", "ajay@gmail", "ajay234", "Delhi", 97734774);
        passengers.add(p);
        Passenger q = new Passenger("Sharat", "sharat@gmail", "sharat234", "Bengaluru", 463763793);
        passengers.add(q);
        Passenger r = new Passenger("AlFareed", "alfareed@gmail", "alfareed234", "Mumbai", 534626498);
        passengers.add(r);
        Flight f = new Flight("AB12X", "KingFisher", "Mumbai", "Bangalore", 50, LocalDate.of(2022, 10, 10), LocalTime.of(11, 11, 00));
        Flight f1 = new Flight("AXF12", "Indigo", "Bangalore", "Hyderabad", 50, LocalDate.of(2023, 9, 1), LocalTime.of(01, 00, 00));
        flights.add(f);
        flights.add(f1);
        initiate ink = new initiate();
        ink.start(passengers, flights);
    }
}
